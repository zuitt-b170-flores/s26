/*
	miniactivity - load the http module and store it in a variable called http
				 - use 4000 as a value of a variable called port
				 - use server variable as a storage of the createServer component
*/
let http = require("http");

const port = 4000

const server = http.createServer((request,response)=>{
	if (request.url === "/greeting") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Hello World");
		/*
			Practice - create two more uri's and let your users see the message "Welcome to ____ Page";
			use successful status code and plain text as the content
		*/
	}
	else if (request.url === "/home") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to the Home Page");
	}
	else if (request.url === "/contact") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("This is Contact Page");
	}
	else{
		response.writeHead(404, {"Content-Type": "text/plain"});
		response.end("Page not found");
	}
});

server.listen(port);

console.log(`Server now running at localhost: ${port}`);



